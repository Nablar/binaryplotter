import math
import numpy as np
import plotly.plotly as py
import plotly.graph_objs as go
from plotly.offline import plot
import plotly.io

lwidth = 0.5
PLOT_LEGENDS = True
TITLE = 'ELF/PE visualizer'


def set_title(title):
    """Function that sets the name of the binary file that is visualized"""
    global TITLE
    TITLE = title

def legends(bool):
    """Function that sets the boolean that controls if the legends of the sections are shown or not"""
    global PLOT_LEGENDS
    PLOT_LEGENDS = bool


def coordinates(n, hexa):
    """Function that computes the coordinates of the n first hexadecimal characters of a string hexa"""
    x, y = 0, 0      # coordinates of the current point
    x_tab, y_tab = [x], [y]  # tabs that will contain all the coordinates
    n = np.min((n, len(hexa)))  # to be sure that n is not bigger than the length of the string hexa
    for k in range(n):
        digit = int(hexa[k], 16)  # transform hex in int for the computation next line
        radian = math.pi / 2 - 2 * math.pi * digit / 16  # digits ordered clockwise, regularly placed on a circle
        x += math.cos(radian)
        y += math.sin(radian)
        x_tab.append(x)
        y_tab.append(y)
    return x_tab, y_tab

def twoDLegendPlotly():
    """Function that returns the legend graph of the 16 directions"""
    # Compute the coordinates
    x_tab, y_tab, text_legend = [0], [0], [None]
    for k in range (16):
        radian = math.pi / 2 - 2 * math.pi * k / 16  # digits ordered clockwise, regularly placed on a circle
        x = math.cos(radian)
        y = math.sin(radian)
        x_tab.append(x)
        y_tab.append(y)
        text_legend.append(hex(k)[2:])
    # Prepare the data for the graph
    curve = go.Scatter(
        x=x_tab, y=y_tab,
        marker = dict(size = 2, color = 'red'),
        mode="markers+text",
        text=text_legend,
        hoverinfo='name',
        name='Legend',
        xaxis='x2',
        yaxis='y2',
        )
    return curve


def twoDRepPlotly(n, hexa, title):
    """Function that prepares the data for the graph for one section of a binary file in 2D, using Plotly"""
    x_tab, y_tab = coordinates(n, hexa)
    curve = go.Scatter(
        x=x_tab, y=y_tab,
        mode="lines",
        text=np.linspace(0, n, n + 1),   # the hooverinfo is the position number of the digit point in the section
        hoverinfo='text',
        name=title)
    return curve


def twoDRepPlotlyShow(curves):
    """Function that plots the visual representation of all the sections of a binary file in 2D, using Plotly"""
    config = {'scrollZoom': True}
    layout = go.Layout(
        xaxis=dict(showgrid=False, zeroline=False, showline=False, showticklabels=False, domain=[0.2, 1]),
        yaxis=dict(showgrid=False, zeroline=False, showline=False, showticklabels=False),
        title=TITLE,
        xaxis2=dict(domain=[0, 0.1], showticklabels=False),
        yaxis2=dict(anchor='x2', domain=[0,0.2], showticklabels=False)
        )
    figure = go.Figure(data=curves, layout=layout)
    plot(figure, config=config, filename='ELF_PE_Visualizer.html')
