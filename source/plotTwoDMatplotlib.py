import math
import numpy as np
import matplotlib.pyplot as plt

PLOT_LEGENDS = True
TITLE = 'ELF/PE visualizer'


def set_title(title):
    """Function that takes the name of the binary file that is visualized"""
    global TITLE
    TITLE = title

def legends(bool):
    """Function that sets the boolean that controls if the legends of the sections are shown or not"""
    global PLOT_LEGENDS
    PLOT_LEGENDS = bool


def twoDInitMatplotlib():
    """Function that creates the figure"""
    global figure
    figure = plt.figure()


def coordinates(n, hexa):
    """Function that computes the coordinates of the n first hexadecimal characters of a string hexa"""
    x, y = 0, 0      # coordinates of the current point
    x_tab, y_tab = [x], [y]  # tabs that will contain all the coordinates
    n = np.min((n, len(hexa)))  # to be sure that n is not bigger than the length of the string hexa
    for k in range(n):
        digit = int(hexa[k], 16)  # transform hex in int for the computation next line
        radian = math.pi / 2 - 2 * math.pi * digit / 16  # digits ordered clockwise, regularly placed on a circle
        x += math.cos(radian)
        y += math.sin(radian)
        x_tab.append(x)
        y_tab.append(y)
    return x_tab, y_tab

def twoDLegendMatplotlib():
    """Function that creates the legend graph of the 16 directions"""
    global figure
    axes2 = figure.add_axes([0.002, 0.002, 0.13, 0.13])
    plt.gca().xaxis.set_major_locator(plt.NullLocator())
    plt.gca().yaxis.set_major_locator(plt.NullLocator())
    axes2.patch.set_color('lightyellow')
    # Compute the coordinates and plot them
    for k in range (16):
        radian = math.pi / 2 - 2 * math.pi * k / 16  # digits ordered clockwise
        x = math.cos(radian)
        y = math.sin(radian)
        axes2.scatter(x, y, color='b', s = 1)
        axes2.text(x,y, hex(k)[2:], size=6)
        axes2.plot([0,x], [0,y], color='g', linewidth=0.5)
    axes2.scatter(0, 0, color='r', s = 2)  # center
    plt.title("Legend", size = 8)

def twoDRepMatplotlib(n, hexa, title):
    """Function that plots the visual representation of one section of a binary file in 2D, using Matplotlib"""
    x_tab, y_tab = coordinates(n, hexa)
    plt.plot(x_tab, y_tab, linewidth=0.5, label=title)


def twoDRepMatplotlibShow():
    """Function that plots and shows the visual representation of all the sections of a binary file in 2D,
    using Matplotlib """
    global figure
    if PLOT_LEGENDS:
        plt.legend(bbox_to_anchor=(0,1), loc="upper left",  bbox_transform=figure.transFigure)
    plt.title(TITLE)
    plt.gca().xaxis.set_major_locator(plt.NullLocator())
    plt.gca().yaxis.set_major_locator(plt.NullLocator())
    twoDLegendMatplotlib() # show the legend of the 16 directions
    plt.show()
